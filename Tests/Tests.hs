{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Test.Framework (Test, defaultMain)

import Tests.EuclideanTests
import Tests.StructureTests

main :: IO () 
main = defaultMain $
  euclideanTests ++ structureTests

