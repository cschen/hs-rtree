{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Tests.StructureTests where

import Test.Framework (Test, defaultMain, plusTestOptions)
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.Framework.Options
import Test.QuickCheck
import qualified Test.QuickCheck.Property as P

import Data.List (group, sort)
import Data.Monoid (mempty)
import RTree
import RTree.TypeClasses

structureTests :: [Test]
structureTests = map (plusTestOptions mempty {
    topt_maximum_test_size = Just 500,
    topt_maximum_generated_tests = Just 1000
  }) [
    testProperty "Correct empty toList" correctEmptyToList,
    testProperty "Correct list contents" correctListContents,
    testProperty "Constant height" constantHeight
  ]

instance Intersectable () () where intersects () () = True
instance BoundingBox () where
  union () () = ()
  split minFill ts = (take minFill ts, drop minFill ts)
  chooseSubtree _ (t:ts) = (t, ts)
instance HasBoundingBox Int where
  type BoundingBoxFor Int = ()
  getBB = const ()

correctEmptyToList :: Gen Bool
correctEmptyToList =
  return $ null $ (toList empty :: [Int])

correctListContents :: Gen P.Result
correctListContents = do
  values :: [Int] <- listOf arbitrary
  let finalTree :: RTreeDefault Int = foldr insert empty values
  return $ (P.liftBool $ sort (toList finalTree) == sort values)
    {P.reason = "values = " ++ show (sort values) ++ "\ntoList tree = " ++ show (sort $ toList finalTree)}

constantHeight :: Gen P.Result
constantHeight = do
  values :: [Int] <- listOf arbitrary
  let finalTree :: RTreeDefault Int = foldr insert empty values
  return $ (P.liftBool $ isConstantHeight finalTree)
    {P.reason = "values = " ++ show (sort values) ++ "\n tree = " ++ show finalTree}
  where
  isConstantHeight :: (HasBoundingBox o) => RTree f o -> Bool
  isConstantHeight t = length (group $ heights t) <= 1
  heights Empty = []
  heights (Root r) = heights' r
  heights' (Leaf x) = [0]
  heights' (Branch _ ts) = map (+1) $ concatMap heights' ts
