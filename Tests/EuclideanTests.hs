{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Tests.EuclideanTests where

import Test.Framework (Test, defaultMain, plusTestOptions)
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.Framework.Options
import Test.QuickCheck
import qualified Test.QuickCheck.Property as P

import Data.Basis
import Data.List (group, sort)
import Data.Monoid (mempty)
import RTree.Euclidean
import RTree.TypeClasses

-- Helper equality function for comparing split outputs
(=?=) :: (Eq a, Show a, Ord a) => (a, [a]) -> (a, [a]) -> Property
(a, b) =?= (a', b') = (a, sort b) === (a', sort b')
(=??=) :: (Eq a, Show a, Ord a) => ([a], [a]) -> ([a], [a]) -> Property
(a, b) =??= (a', b') = (sort a, sort b) === (sort a', sort b')

euclideanTests :: [Test]
euclideanTests =
  [ testProperty "Rect volume" $ volume (MBR (2, 4) (7, 10) :: Rect) === 30
  , testProperty "Rect surface area" $ surfaceArea (MBR (2, 4) (7, 10) :: Rect) === 11
  , testProperty "Rectpris volume" $ volume (MBR (2, 4, 6) (7, 10, 13) :: RectPris) === 210
  , testProperty "Rectpris surface area" $ surfaceArea (MBR (2, 4, 6) (7, 10, 13) :: RectPris) === 107
  , testProperty "Rect chooseSubtree; exactly one deletion" $
    chooseSubtree (MBR (0,0) (1,1)) [MBR (0,0) (1,1), MBR (0,0) (1,1) :: Rect]
    =?= (MBR (0,0) (1,1), [MBR (0,0) (1,1)])
  , testProperty "Rectpris chooseSubtree; exactly one deletion" $
    chooseSubtree (MBR (0,0,0) (1,1,1)) [MBR (0,0,0) (1,1,1), MBR (0,0,0) (1,1,1) :: RectPris]
    =?= (MBR (0,0,0) (1,1,1), [MBR (0,0,0) (1,1,1)])
  , testProperty "Rect split by x" $
    split 2 [
        MBR (0, 0) (1, 1) :: Rect,
        MBR (2, 0) (3, 1),
        MBR (0, 1) (1, 2),
        MBR (2, 1) (3, 2)]
    =??= ( [MBR (0, 0) (1, 1), MBR (0, 1) (1, 2)],
          [MBR (2, 0) (3, 1), MBR (2, 1) (3, 2)])
  , testProperty "Rect split by y" $
    split 2 [
        MBR (0, 0) (1, 1) :: Rect,
        MBR (1, 0) (2, 1),
        MBR (0, 2) (1, 3),
        MBR (1, 2) (2, 3)]
    =??= ( [MBR (0, 0) (1, 1), MBR (1, 0) (2, 1)],
          [MBR (0, 2) (1, 3), MBR (1, 2) (2, 3)])
  , testProperty "Rect split by x, choose by volume" $
    split 2 [
        MBR (0, 0) (1, 1) :: Rect,
        MBR (7, 0) (8, 1),
        MBR (0, 1) (1, 2),
        MBR (7, 1) (8, 2),
        MBR (3, 0) (4, 2)]
    =??= ( [MBR (0, 0) (1, 1), MBR (0, 1) (1, 2), MBR (3, 0) (4, 2)],
          [MBR (7, 0) (8, 1), MBR (7, 1) (8, 2)])
    .&&.
    split 2 [
        MBR (0, 0) (1, 1) :: Rect,
        MBR (7, 0) (8, 1),
        MBR (0, 1) (1, 2),
        MBR (7, 1) (8, 2),
        MBR (4, 0) (5, 2)]
    =??= ( [MBR (0, 0) (1, 1), MBR (0, 1) (1, 2)],
          [MBR (7, 0) (8, 1), MBR (7, 1) (8, 2), MBR (4, 0) (5, 2)])
  , testProperty "Rect split by y, choose by volume" $
    split 2 [
        MBR (0, 0) (1, 1) :: Rect,
        MBR (0, 7) (1, 8),
        MBR (1, 0) (2, 1),
        MBR (1, 7) (2, 8),
        MBR (0, 3) (2, 4)]
    =??= ( [MBR (0, 0) (1, 1), MBR (1, 0) (2, 1), MBR (0, 3) (2, 4)],
          [MBR (0, 7) (1, 8), MBR (1, 7) (2, 8)])
    .&&.
    split 2 [
        MBR (0, 0) (1, 1) :: Rect,
        MBR (0, 7) (1, 8),
        MBR (1, 0) (2, 1),
        MBR (1, 7) (2, 8),
        MBR (0, 4) (2, 5)]
    =??= ( [MBR (0, 0) (1, 1), MBR (1, 0) (2, 1)],
          [MBR (0, 7) (1, 8), MBR (1, 7) (2, 8), MBR (0, 4) (2, 5)])
  ] ++ helperSanityChecks

helperSanityChecks =
  [ testProperty "=??= helper sanity" $ expectFailure $ ([1], [2]) =??= ([2], [1])
  , testProperty "=??= helper sanity" $ ([1], [2]) =??= ([1], [2])
  , testProperty "=??= helper sanity" $ ([1, 0], [2]) =??= ([0, 1], [2])
  , testProperty "=??= helper sanity" $ ([1, 0, 7], [2, 7, 3]) =??= ([0, 7, 1], [7, 3, 2])
  , testProperty "=?= helper sanity" $ expectFailure $ (1, [2]) =?= (2, [1])
  , testProperty "=?= helper sanity" $ (1, [2, 7, 3]) =?= (1, [7, 3, 2])
  ]

instance HasBasis Int where
  type Basis Int = ()
  basisValue () = 1
  decompose s = [((), s)]
  decompose' = const

type Rect = (MBR (Int, Int))
type RectPris = (MBR (Int, Int, Int))
instance HasBoundingBox Rect where
  type BoundingBoxFor Rect = Rect
  getBB = id
instance HasBoundingBox RectPris where
  type BoundingBoxFor RectPris = RectPris
  getBB = id
