{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module RTree (
  RTree(..),
  RTreeNode(..),
  FillOptions(..),
  RTreeDefault,
  empty, singleton,
  insert,
  delete,
  deleteBy,
  toList,
  -- TRAVERSAL
  prunedFoldMap,
  prunedSearch,
  itemsIntersecting,
  ) where

import Data.Foldable (Foldable(foldMap))
import Data.Monoid (Monoid, mempty, mconcat)
import Data.Proxy
import Data.Reflection

import RTree.TypeClasses

data FillOptions = FillOptions {minFill :: Int, maxFill :: Int}
  deriving (Show, Eq)
data DefaultFillOptions
instance Reifies DefaultFillOptions FillOptions where
  reflect = const $ FillOptions {minFill = 50, maxFill = 100}

-- R-trees with fill options @f@, bounding box type @b@, object type @o@.
data (Reifies f FillOptions, HasBoundingBox o) => RTree f o
  = Empty
  | Root (RTreeNode o)
data RTreeNode o
  = Leaf !o
  | Branch !(BoundingBoxFor o) [RTreeNode o]
type RTreeDefault = RTree DefaultFillOptions

empty = Empty
singleton :: (Reifies f FillOptions, HasBoundingBox o) => o -> RTree f o
singleton = Root . Leaf

instance (HasBoundingBox o) => HasBoundingBox (RTreeNode o) where
  type BoundingBoxFor (RTreeNode o) = BoundingBoxFor o
  getBB = getNodeBB
getNodeBB :: (HasBoundingBox o, b ~ BoundingBoxFor o) => RTreeNode o -> BoundingBoxFor o
getNodeBB (Leaf o) = getBB o
getNodeBB (Branch b _) = b

-- insertion, deletion

data InsertResult a
  = One a
  | Two a a

insert :: forall f o .
  (Reifies f FillOptions, HasBoundingBox o) => o -> RTree f o -> RTree f o
insert o Empty = singleton o
insert o (Root r) = Root $ case insert' (reflect (Proxy :: Proxy f)) o r of
  One t     -> t
  Two t1 t2 -> branch [t1, t2]

insert' :: forall o . (HasBoundingBox o) => FillOptions -> o -> RTreeNode o -> InsertResult (RTreeNode o)
insert' _ o l@(Leaf _) = Two l $ Leaf o
insert' f o (Branch _ ts) =
  let
    (t, ts') = chooseSubtree (getBB o) ts
    newTs = case insert' f o t of
      One t1    -> t1 : ts'
      Two t1 t2 -> t1 : t2 : ts'
  in
  if length newTs <= maxFill f
  then One $ branch newTs
  else let (ts1, ts2) = split (minFill f) newTs in Two (branch ts1) (branch ts2)

branch :: (HasBoundingBox o) => [RTreeNode o] -> RTreeNode o
branch ts = Branch (foldr1 union $ map getNodeBB ts) ts

deleteBy :: (o -> Bool) -> RTree f o -> RTree f o
deleteBy = undefined
delete :: (Eq o) => o -> RTree f o -> RTree f o
delete = deleteBy . (==)

-- traversal

toList :: (HasBoundingBox o) => RTree f o -> [o]
toList = prunedFoldMap (const True, const True) (:[])

prunedFoldMap :: (HasBoundingBox o, Monoid m) => ((BoundingBoxFor o) -> Bool, o -> Bool) -> (o -> m) -> RTree f o -> m
-- First argument: (isBoundingBoxFeasible, doesObjectMatch)
prunedFoldMap _ _ Empty = mempty
prunedFoldMap algs f (Root r) = prunedFoldMap' algs f r
prunedFoldMap' :: (Monoid m) => (BoundingBoxFor o -> Bool, o -> Bool) -> (o -> m) -> RTreeNode o -> m
prunedFoldMap' (_, h) f (Leaf o)
  | h o       = f o
  | otherwise = mempty
prunedFoldMap' algs@(g, _) f (Branch b ts)
  | g b       = mconcat $ map (prunedFoldMap' algs f) ts
  | otherwise = mempty

prunedSearch :: HasBoundingBox o => (BoundingBoxFor o -> Bool, o -> Bool) -> RTree f o -> [o]
prunedSearch = flip prunedFoldMap (: [])

itemsIntersecting :: (HasBoundingBox o, Intersectable x (BoundingBoxFor o), Intersectable x o) => x -> RTree f o -> [o]
itemsIntersecting x = prunedSearch (intersects x, intersects x)

instance (Reifies f FillOptions, HasBoundingBox o, Show o, Show (BoundingBoxFor o)) => Show (RTree f o) where
  show :: RTree f o -> String
  show rt = "RangeTree (" ++ (show $ (reflect (Proxy :: Proxy f) :: FillOptions)) ++ ", " ++ pprRT rt ++ ")"
instance (Show o, Show (BoundingBoxFor o)) => Show (RTreeNode o) where
  show :: RTreeNode o -> String
  show (Leaf o) = "Leaf " ++ (show o)
pprRT :: (HasBoundingBox o, Show o, Show (BoundingBoxFor o)) => RTree f o -> String
pprRT (Empty) = "Empty"
pprRT (Root r) = "Root (" ++ show r ++ ")"
