{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE TypeFamilies #-}

module RTree.TypeClasses where

{- | The 'BoundingBox' class denotes types that represent regions in space (for
example, minumum bounding rectangles, bounding spheres, or convex hulls).
Instances of 'BoundingBox' should satisfy the following laws:

> a `union` (b `union` c) == (a `union` b) `union` c
> a `union` b == b `union` c
> a `intersects` (a `union` b)
-}

class Intersectable o1 o2 where
  intersects :: o1 -> o2 -> Bool

class (Intersectable b b) => BoundingBox b where
  union :: b -> b -> b
  split :: SplitAlgorithm b
  chooseSubtree :: ChooseSubtreeAlgorithm b
  contains :: b -> b -> Bool
  default contains :: Eq b => b -> b -> Bool
  x `contains` y = (x `union` y) == y

-- Splitting function for dividing two sets of bounding boxes into two nodes,
-- given a certain minFill size.
-- Returns the input objects split into two (non-empty) lists of size >= minFill
type SplitAlgorithm b = forall boxed . (HasBoundingBox boxed, b ~ BoundingBoxFor boxed) => Int -> [boxed] -> ([boxed], [boxed])

-- Function for deciding which child to add an object into.
-- Returns a pair: the selected subtree to descend into, and its siblings.
type ChooseSubtreeAlgorithm b = forall boxed . (HasBoundingBox boxed, b ~ BoundingBoxFor boxed) => b -> [boxed] -> (boxed, [boxed])

{- |
The 'BoundingBoxFor' class denotes that one type can serve as a bounding box
for another.

Instances of both 'Intersectable o b' and 'BoundingBoxFor b o' should
additionally satisfy the law:

> x `intersects` (getBB x)

Instances of 'Intersectable o1 b', 'Intersectable o1 o2' and
'BoundingBoxFor b o2' should additionally satisfy the law:

> (x::o1 `intersects` y::o2) ==> (x `intersects` getBB y)
-}
class (BoundingBox (BoundingBoxFor o)) => HasBoundingBox o where
  type BoundingBoxFor o
  getBB :: o -> (BoundingBoxFor o)

