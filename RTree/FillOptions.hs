{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

module RTree.FillOptions where

import Data.Reflection

class FillOptions s where
  minFill :: proxy s -> Int
  maxFill :: proxy s -> Int
data DefaultFillOptions
instance FillOptions DefaultFillOptions where
  minFill = const 50
  maxFill = const 100
data GivenFillOptions = GivenFillOptions Int Int
instance (Given GivenFillOptions) => FillOptions GivenFillOptions where
  minFill = let GivenFillOptions a _ = given in const a
  maxFill = let GivenFillOptions _ b = given in const b
withFillOptions :: Int -> Int -> forall r. (FillOptions GivenFillOptions => r) -> r
withFillOptions minFill_ maxFill_ f = give (GivenFillOptions minFill_ maxFill_) f
