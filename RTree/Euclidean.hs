{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

-- TODO: rename
-- Module for default R*-tree implementation for minimum bounding rectangles in Euclidean space
module RTree.Euclidean (
  MBR(..),
#if TEST
  volume,
  surfaceArea,
#endif
) where

import Data.Basis
import Data.Function (on)
import Data.List (deleteBy, minimumBy, sortBy)
import Data.Monoid
import Data.Ord (comparing)
import Data.VectorSpace
import qualified Data.List as L
import RTree.TypeClasses
import Prelude

-- zips e.g. dot product is zipAndFold (*) (+)
--zipAndFold :: (Scalar pt -> Scalar pt -> a) -> (a -> a -> a) -> pt -> pt -> a

-- *Inclusive* bounding box.
-- minimum coords, maximum coords

data MBR v = MBR v v deriving (Eq, Show, Ord)
minCoord (MBR a _) = a
maxCoord (MBR _ b) = b

getComponents :: HasBasis v => v -> [Scalar v]
getComponents = map snd . decompose

-- The main conceit here is we assume that decompose always produces the basis in the same order.
basisOrder :: HasBasis v => v -> [Basis v]
basisOrder = map fst . decompose

vMin :: (HasBasis v, Ord (Scalar v)) => v -> v -> v
vMin u v =
  recompose $ zip (basisOrder u) $ (zipWith min `on` getComponents) u v
vMax :: (HasBasis v, Ord (Scalar v)) => v -> v -> v
vMax u v =
  recompose $ zip (basisOrder u) $ (zipWith max `on` getComponents) u v

instance (HasBasis v, Ord (Scalar v)) => Intersectable (MBR v) (MBR v) where
  intersects (MBR a b) (MBR c d) =
    and $ (zipWith (<=) `on` getComponents) (a `vMax` c) (b `vMin` d)
instance (HasBasis v, Ord (Scalar v), Num (Scalar v), Eq v) => BoundingBox (MBR v) where
  union (MBR a b) (MBR c d) = MBR (a `vMin` c) (b `vMax` d)
  chooseSubtree = chooseMinimalAreaEnlargement
  split :: forall boxed . (HasBoundingBox boxed, MBR v ~ BoundingBoxFor boxed) => Int -> [boxed] -> ([boxed], [boxed])
  split minFill bs = let
      n = length bs
      splitAxisMarginSum :: Int -> Scalar v
      splitAxisMarginSum i = sum $
        map (uncurry ((+) `on` (surfaceArea . foldr1 union . map getBB))) $ axisDistributions i
        -- TODO: this is O(N^2)
      axisDistributions :: Int -> [([boxed], [boxed])]
      axisDistributions i =
        map (\j -> (take j $ axisSorted i, drop j $ axisSorted i))
          [minFill .. n - minFill]
      axisSorted :: Int -> [boxed]
      axisSorted i = sortBy
            (comparing ((!!i) . getComponents . minCoord . getBB) `mappend`
              comparing ((!!i) . getComponents . maxCoord . getBB)) bs
      MBR arbitraryPt _ = getBB $ head bs
      splitAxis = minimumBy (comparing splitAxisMarginSum) [0..length (decompose arbitraryPt) - 1]
      unionOf = foldr1 union
    in minimumBy ((comparing (uncurry ((+) `on` volume))  `mappend` comparing (volume . uncurry union))
      `on` (\(a, b) -> (unionOf $ map getBB a, unionOf $ map getBB b))) $ axisDistributions splitAxis
    -- TODO: this too is O(N^2)

volume :: (HasBasis v, Num (Scalar v)) => MBR v -> Scalar v
volume (MBR a b) = product $ getComponents $ b ^-^ a

surfaceArea :: (HasBasis v, Num (Scalar v)) => MBR v -> Scalar v
surfaceArea (MBR a b) = let
    comps = getComponents $ b ^-^ a
    z1 = init $ scanl (*) 1 comps
    z2 = tail $ scanr (*) 1 comps
  in sum $ L.zipWith (*) z1 z2

chooseMinimalAreaEnlargement ::
  forall boxed v . (HasBasis v, HasBoundingBox boxed, Eq v, Num (Scalar v), Ord (Scalar v), MBR v ~ BoundingBoxFor boxed) =>
  MBR v -> [boxed] -> (boxed, [boxed])
chooseMinimalAreaEnlargement newMbr mbrs =
  (best, deleteBy ((==) `on` getBB) best mbrs)
  where
  statsFor :: MBR v -> (Scalar v, Scalar v)
  statsFor mbr = let currentArea = volume mbr in
    (volume (newMbr `union` mbr) - currentArea, currentArea)
  best :: boxed
  best = minimumBy (compare `on` statsFor . getBB) mbrs
